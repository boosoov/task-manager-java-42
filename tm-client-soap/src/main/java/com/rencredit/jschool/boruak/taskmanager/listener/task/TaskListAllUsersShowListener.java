package com.rencredit.jschool.boruak.taskmanager.listener.task;

import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.util.ViewUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TaskListAllUsersShowListener extends AbstractListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-list-all";
    }

    @NotNull
    @Override
    public String description() {
        return "Show tasks list all users.";
    }

    @Override
    @EventListener(condition = "@taskListAllUsersShowListener.name() == #event.command")
    public void handle(final ConsoleEvent event) throws EmptyUserException, EmptyUserIdException_Exception, UnknownUserException_Exception, DeniedAccessException_Exception {
        System.out.println("[LIST TASKS ALL USERS]");

        @NotNull final List<TaskDTO> tasks = taskEndpoint.getListAllUsersTasks();
        @NotNull int index = 1;
        for (@NotNull final TaskDTO task : tasks) {
            System.out.println(index + ". ");
            ViewUtil.showTask(task);
            index++;
        }
        System.out.println("[OK]");
    }

}
