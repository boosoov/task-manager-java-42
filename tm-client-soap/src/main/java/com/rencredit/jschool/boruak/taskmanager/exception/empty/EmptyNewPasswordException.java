package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyNewPasswordException extends AbstractClientException {

    public EmptyNewPasswordException() {
        super("Error! New password is empty...");
    }

}
