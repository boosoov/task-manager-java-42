package com.rencredit.jschool.boruak.taskmanager.exception.unknown;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class UnknownUserException extends AbstractClientException {

    public UnknownUserException() {
        super("Error! Unknown user...");
    }

    public UnknownUserException(final String user) {
        super("Error! Unknown user ``" + user + "``...");
    }

}
