package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyMiddleNameException extends AbstractClientException {

    public EmptyMiddleNameException() {
        super("Error! Middle name is empty...");
    }

}
