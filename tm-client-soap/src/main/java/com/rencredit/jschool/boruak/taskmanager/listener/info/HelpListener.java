package com.rencredit.jschool.boruak.taskmanager.listener.info;

import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyCommandException;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.service.InfoService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class HelpListener extends AbstractListener {

    @Autowired
    private InfoService infoService;

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Display terminal command.";
    }

    @NotNull
    @Autowired
    private AbstractListener[] abstractListeners;

    @Override
    @EventListener(condition = "@helpListener.name() == #event.command")
    public void handle(final ConsoleEvent event) throws EmptyCommandException {
        System.out.println("[HELP]");
        for (@NotNull final AbstractListener command : abstractListeners) {
            System.out.println(command.name()
                    + (command.arg() != null ? ", " + command.arg() + " - " : " - ")
                    + command.description());
        }
        System.out.println("[OK]");
    }

}
