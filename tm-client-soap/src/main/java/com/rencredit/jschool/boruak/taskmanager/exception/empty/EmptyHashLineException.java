package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyHashLineException extends AbstractClientException {

    public EmptyHashLineException() {
        super("Error! Line for hash is empty...");
    }

}
