package restendpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.ITasksRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ITasksRestEndpointTest {

    ITasksRestEndpoint tasksRestEndpoint;

    @Before
    public void init()  {
        tasksRestEndpoint = ITasksRestEndpoint.client("http://localhost:8080/");
    }

    @After
    public void clearAll() {

    }

    @Test
    public void test() {
        tasksRestEndpoint.deleteAll();
        List<TaskDTO> emptyList = tasksRestEndpoint.getListDTO();
        Assert.assertTrue(emptyList.isEmpty());
        List<TaskDTO> listForSave = new ArrayList<>();
        listForSave.add(new TaskDTO());
        listForSave.add(new TaskDTO());
        listForSave.add(new TaskDTO());
        tasksRestEndpoint.saveAll(listForSave);
        Assert.assertEquals(3, tasksRestEndpoint.count());
        Assert.assertEquals(3, tasksRestEndpoint.getListDTO().size());
        tasksRestEndpoint.deleteAll(listForSave);
        Assert.assertEquals(0, tasksRestEndpoint.count());
    }

}
