package com.rencredit.jschool.boruak.taskmanager.entity;

import com.rencredit.jschool.boruak.taskmanager.enumerated.Status;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Setter
@Getter
public class Task extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @Nullable
    private String name;

    @Nullable
    private String description = "";

    private Status status = Status.NOT_STARTED;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Nullable
    private User user;

    @Nullable
    private Project project;

    public Task() {
    }

    public Task(
            @NotNull final User user,
            @NotNull final Project project,
            @NotNull final String name

    ) {
        this.user = user;
        this.project = project;
        this.name = name;
    }

    public Task(
            @NotNull final User user,
            @NotNull final Project project,
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.user = user;
        this.project = project;
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id='" + super.getId() + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", userId='" + user + '\'' +
                '}';
    }

}
