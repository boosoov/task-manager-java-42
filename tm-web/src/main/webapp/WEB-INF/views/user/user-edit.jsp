<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%--<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>--%>
<jsp:include page="../../include/_header.jsp"/>

<h2>USER EDIT</h2>

<form:form action="/user/edit" method="POST" modelAttribute="userForEdit">
    <form:input type="hidden" path="id" />

    USER: <sec:authentication property="name"/>

    <p>
        <div style="margin-bottom: 5px;">PASSWORD:</div>
        <div><form:input type="text" path="passwordHash" /></div>
    </p>

    <p>
        <div style="margin-bottom: 5px;">EMAIL:</div>
        <div><form:input type="text" path="email" /></div>
    </p>

    <p>
        <div style="margin-bottom: 5px;">FIRST NAME:</div>
        <div><form:input type="text" path="firstName" /></div>
    </p>

    <p>
        <div style="margin-bottom: 5px;">LAST NAME:</div>
        <div><form:input type="text" path="lastName" /></div>
    </p>

    <p>
        <div style="margin-bottom: 5px;">MIDDLE NAME:</div>
        <div><form:input type="text" path="middleName" /></div>
    </p>

    <button type="submit">SAVE USER</button>

</form:form>



<jsp:include page="../../include/_footer.jsp"/>

