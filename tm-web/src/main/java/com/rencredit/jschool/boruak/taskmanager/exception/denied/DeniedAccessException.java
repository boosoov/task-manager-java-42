package com.rencredit.jschool.boruak.taskmanager.exception.denied;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class DeniedAccessException extends AbstractException {

    public DeniedAccessException() {
        super("Access denied...");
    }

}
