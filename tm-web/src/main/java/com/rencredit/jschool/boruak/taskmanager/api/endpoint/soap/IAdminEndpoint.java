package com.rencredit.jschool.boruak.taskmanager.api.endpoint.soap;

import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyRoleException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptySessionException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.jws.WebMethod;
import java.io.IOException;

public interface IAdminEndpoint {

    @WebMethod
    String getHostPort() throws DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, UnknownUserException;

    @WebMethod
    String getHost() throws DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, UnknownUserException;

    @WebMethod
    Integer getPort() throws DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, UnknownUserException;

    @WebMethod
    void shutDownServer() throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, UnknownUserException;

}
