package com.rencredit.jschool.boruak.taskmanager.exception.unknown;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Error! Unknown command...");
    }

    public UnknownCommandException(final String command) {
        super("Error! Unknown command ``" + command + "``...");
    }

}
