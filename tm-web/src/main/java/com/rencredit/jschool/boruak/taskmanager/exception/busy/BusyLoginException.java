package com.rencredit.jschool.boruak.taskmanager.exception.busy;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class BusyLoginException extends AbstractException {

    public BusyLoginException() {
        super("Error! Login is busy...");
    }

}
