package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectHashPasswordException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IUserService {

    void save(@Nullable final String login, @Nullable final String password) throws EmptyLoginException, EmptyPasswordException, EmptyHashLineException, DeniedAccessException, EmptyUserException, BusyLoginException;

    void save(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws EmptyLoginException, EmptyPasswordException, EmptyRoleException, EmptyHashLineException, DeniedAccessException, BusyLoginException, EmptyUserException;

    void save(@Nullable final User user) throws EmptyLoginException, EmptyUserException, BusyLoginException;

    void save(@Nullable final UserDTO user) throws EmptyUserException, BusyLoginException, EmptyLoginException;

    @Transactional
    void rewrite(@Nullable final UserDTO user) throws EmptyUserException, EmptyLoginException, UnknownUserException;

    @Nullable
    UserDTO findByIdDTO(@Nullable final  String id) throws EmptyIdException;

    @Nullable
    User findByIdEntity(@Nullable final String s) throws EmptyIdException;

    @Nullable
    User findByLoginEntity(@Nullable final String login) throws EmptyLoginException;

    @Nullable
    UserDTO findByLoginDTO(@Nullable final String login) throws EmptyLoginException;

    @NotNull
    List<UserDTO> findListDTO();

    @Nullable
    UserDTO updatePasswordById(@Nullable final String id, @Nullable final String newPassword) throws EmptyUserException, IncorrectHashPasswordException, EmptyHashLineException, EmptyNewPasswordException, EmptyIdException;

    @Nullable
    UserDTO lockUserByLogin(@Nullable final String login) throws EmptyUserException, EmptyLoginException;

    @Nullable
    UserDTO unlockUserByLogin(@Nullable final String login) throws EmptyUserException, EmptyLoginException;

    void deleteAll() throws EmptyPasswordException, EmptyUserException, EmptyLoginException, DeniedAccessException, BusyLoginException, EmptyHashLineException, EmptyRoleException;

    void deleteById(@Nullable final String id) throws EmptyIdException;

    void deleteByLogin(@Nullable final String login) throws EmptyLoginException;

    void deleteByUser(@Nullable final UserDTO user) throws EmptyUserException, EmptyIdException;

}
