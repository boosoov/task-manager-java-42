package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectHashPasswordException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.repository.dto.IUserRepositoryDTO;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.IUserRepositoryEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserService implements IUserService {

    @Autowired
    private IUserRepositoryEntity repositoryEntity;

    @Autowired
    private IUserRepositoryDTO repositoryDTO;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void save(@Nullable final String login, @Nullable final String password) throws EmptyLoginException, EmptyPasswordException, EmptyHashLineException, DeniedAccessException, EmptyUserException, BusyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        @Nullable final String passwordHash = passwordEncoder.encode(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final User user = new User(login, passwordHash);
        save(user);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void save(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws EmptyLoginException, EmptyPasswordException, EmptyRoleException, EmptyHashLineException, DeniedAccessException, BusyLoginException, EmptyUserException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();

        @Nullable final String passwordHash = passwordEncoder.encode(password);
        if (passwordHash == null) throw new DeniedAccessException();
        @NotNull final User user = new User(login, passwordHash, role);
        if (repositoryEntity.findByLogin(user.getLogin()) != null) throw new BusyLoginException();
        repositoryEntity.save(user);
    }

    @Override
    @Transactional
    public void save(@Nullable final User user) throws EmptyUserException, BusyLoginException, EmptyLoginException {
        if (user == null) throw new EmptyUserException();
        if (user.getLogin() == null || user.getLogin().isEmpty()) throw new EmptyLoginException();

        if (repositoryEntity.findByLogin(user.getLogin()) != null) throw new BusyLoginException();
        repositoryEntity.save(user);
    }

    @Override
    @Transactional
    public void save(@Nullable final UserDTO user) throws EmptyUserException, BusyLoginException, EmptyLoginException {
        if (user == null) throw new EmptyUserException();
        if (user.getLogin() == null || user.getLogin().isEmpty()) throw new EmptyLoginException();

        if (repositoryEntity.findByLogin(user.getLogin()) != null) throw new BusyLoginException();
        repositoryDTO.save(user);
    }

    @Override
    @Transactional
    public void rewrite(@Nullable final UserDTO user) throws EmptyUserException, EmptyLoginException, UnknownUserException {
        if (user == null) throw new EmptyUserException();
        if (user.getLogin() == null || user.getLogin().isEmpty()) throw new EmptyLoginException();

        if (repositoryEntity.findById(user.getId()) == null) throw new UnknownUserException();
        repositoryDTO.save(user);
    }

    @Nullable
    @Override
    public UserDTO findByIdDTO(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @Nullable final Optional<UserDTO> opt = repositoryDTO.findById(id);
        return opt.isPresent() ? opt.get() : null;
    }

    @Nullable
    @Override
    public User findByIdEntity(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repositoryEntity.getOne(id);
    }

    @Nullable
    @Override
    public User findByLoginEntity(@Nullable final String login) throws EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return repositoryEntity.findByLogin(login);
    }

    @Nullable
    @Override
    public UserDTO findByLoginDTO(@Nullable final String login) throws EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return repositoryDTO.findByLogin(login);
    }

    @NotNull
    @Override
    public List<UserDTO> findListDTO() {
        @NotNull final List<UserDTO> list = repositoryDTO.findAll();
        return list;
    }

    @Nullable
    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public UserDTO updatePasswordById(@Nullable final String id, @Nullable final String newPassword) throws EmptyUserException, IncorrectHashPasswordException, EmptyHashLineException, EmptyNewPasswordException, EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyNewPasswordException();

        @Nullable final String passwordHash = passwordEncoder.encode(newPassword);
        if (passwordHash == null) throw new IncorrectHashPasswordException();
        @Nullable final UserDTO user = findByIdDTO(id);
        if (user == null) throw new EmptyUserException();
        user.setPasswordHash(passwordHash);
        repositoryDTO.save(user);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public UserDTO lockUserByLogin(@Nullable final String login) throws EmptyUserException, EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        @Nullable final UserDTO user = findByLoginDTO(login);
        if (user == null) throw new EmptyUserException();
        user.setLocked(true);
        repositoryDTO.save(user);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public UserDTO unlockUserByLogin(@Nullable final String login) throws EmptyUserException, EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        @Nullable final UserDTO user = findByLoginDTO(login);
        if (user == null) throw new EmptyUserException();
        user.setLocked(false);
        repositoryDTO.save(user);
        return user;
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteByLogin(@Nullable final String login) throws EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        repositoryEntity.deleteByLogin(login);
    }

    @Override
    @Transactional
    public void deleteByUser(@Nullable final UserDTO user) throws EmptyUserException, EmptyIdException {
        if (user == null) throw new EmptyUserException();

        deleteById(user.getId());
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteAll() throws EmptyPasswordException, EmptyUserException, EmptyLoginException, DeniedAccessException, BusyLoginException, EmptyHashLineException, EmptyRoleException {
        repositoryEntity.deleteAll();
        createDefaultUses();
    }

    @Transactional
    public void createDefaultUses() throws EmptyPasswordException, EmptyUserException, EmptyLoginException, DeniedAccessException, BusyLoginException, EmptyHashLineException, EmptyRoleException {
        this.save("admin", "admin", Role.ADMIN);
        this.save("test", "test");
        this.save("1", "1");
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        repositoryEntity.deleteById(id);
    }

}
