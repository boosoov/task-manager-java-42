package com.rencredit.jschool.boruak.taskmanager.api.endpoint.rest;

import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyProjectListException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/projects")
public interface IProjectsRestEndpoint {

    static IProjectsRestEndpoint client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(IProjectsRestEndpoint.class, baseUrl);
    }

    @GetMapping
    List<ProjectDTO> getListDTO() throws EmptyUserIdException, DeniedAccessException, UnknownUserException;

    @GetMapping("/all")
    List<ProjectDTO> getListDTOAll();

    @PostMapping
    List<ProjectDTO> saveAll(@RequestBody List<ProjectDTO> list) throws DeniedAccessException, UnknownUserException, EmptyProjectListException;

    @GetMapping("/count")
    long count() throws DeniedAccessException, UnknownUserException, EmptyUserIdException;

    @DeleteMapping("/all")
    void deleteAll() throws EmptyUserIdException, DeniedAccessException, UnknownUserException;

}
