package com.rencredit.jschool.boruak.taskmanager.endpoint.rest;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.rest.ITaskRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyTaskException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyTaskIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.service.TaskService;
import com.rencredit.jschool.boruak.taskmanager.util.UserUtil;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private TaskService taskService;

    @Override
    @PostMapping
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void create(@RequestBody TaskDTO task) throws EmptyTaskException, EmptyUserIdException, DeniedAccessException, UnknownUserException {
        task.setUserId(UserUtil.getUserId());
        taskService.save(task);
    }

    @Override
    @Nullable
    @GetMapping("/${id}")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public TaskDTO findOneByIdDTO(@PathVariable("id") String id) throws EmptyIdException, EmptyUserIdException, DeniedAccessException, UnknownUserException {
        return taskService.findOneByIdDTO(UserUtil.getUserId(), id);
    }

    @Override
    @GetMapping("/exist/${id}")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public boolean existsById(@PathVariable("id") String id) throws DeniedAccessException, UnknownUserException, EmptyUserIdException, EmptyTaskIdException {
        return taskService.existsByUserIdAndTaskId(UserUtil.getUserId(), id);
    }

    @Override
    @DeleteMapping("/${id}")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void deleteOneById(@PathVariable("id") String id) throws DeniedAccessException, UnknownUserException, EmptyUserIdException, EmptyIdException {
        taskService.deleteById(UserUtil.getUserId(), id);
    }

}
