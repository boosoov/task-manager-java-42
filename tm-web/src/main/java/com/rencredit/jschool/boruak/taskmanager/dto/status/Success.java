package com.rencredit.jschool.boruak.taskmanager.dto.status;

public class Success extends Result {

    public Success() {
        success = true;
        message = "";
    }

}
